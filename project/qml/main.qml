import QtQuick 2.9
import QtQuick.Window 2.3

Window {
    id: root
    visible: true

    property real sideBarWidth: root.width / 4.553
    title: qsTr("CourseProject")

    visibility: "Maximized"

    Sidebar
    {
        id: sidebar
        width: sideBarWidth
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        states: [
                State {
                    name: "profilePage"
                    PropertyChanges { target: profilePage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: archivePage; opacity: 0; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    },
                State {
                    name: "archivePage"
                    PropertyChanges { target: profilePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: settingPage; opacity: 0 ; enabled: false}
                    },
                State {
                    name: "settingPage"
                    PropertyChanges { target: profilePage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 1; z:2; enabled: true}
                    }
            ]

        transitions: [
                Transition {
                    from: "*"; to: "*"
                    NumberAnimation { target: profilePage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: archivePage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: settingPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                }
            ]

        state: "profilePage"

        property string latestState

        onPatientClicked: { sidebar.state = "profilePage" }
        onArchiveClicked: { sidebar.state = "archivePage" }
        onSettingClicked: {
            latestState = sidebar.state
            sidebar.state = "settingPage"
        }
    }

    ProfilePage{
        id: profilePage
        visible: true
        enabled: false

        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }

    ArchivePage{
        id: archivePage
        visible: true
        enabled: false
        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }

    SettingPage{
        id: settingPage
        visible: true
        enabled: false

        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }
}
