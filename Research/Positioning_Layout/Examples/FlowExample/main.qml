import QtQuick 2.9
import QtQuick.Window 2.3
Window {
    id: root
    visible: true

    BrightSquare {
        width: 160
        height: 160

        Flow {
            anchors.fill: parent
            anchors.margins: 20
            spacing: 20
            RedSquare { }
            BlueSquare { }
            GreenSquare { }
        }
    }
}
