import QtQuick 2.9
import QtQuick.Window 2.3
Window {
    id: root
    visible: true

    DarkSquare {
        width: 120
        height: 240

        Column {
            id: row
            anchors.centerIn: parent
            spacing: 8
            RedSquare { }
            GreenSquare { width: 96 }
            BlueSquare { }
        }
    }
}
