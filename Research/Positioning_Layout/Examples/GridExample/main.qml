import QtQuick 2.9
import QtQuick.Window 2.3
Window {
    id: root
    visible: true

    BrightSquare {
        width: 160
        height: 160

        Grid {
            id: grid
            rows: 2
            columns: 2
            anchors.centerIn: parent
            spacing: 8
            RedSquare { }
            RedSquare { }
            RedSquare { }
            RedSquare { }
        }
     }
}
