// BlueSquare.qml

import QtQuick 2.5

Rectangle
{
    width: 48; height: 48; color: "lightblue"
    property alias text: label.text

    Text { id: label; anchors.centerIn: parent }
}
