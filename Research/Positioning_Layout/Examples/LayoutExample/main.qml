import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    id: root
    visible: true
    Rectangle {
        width: 400; height: 300; color: "darkgray"

        Grid {
            x: 10; y: 10
            rows: 2; columns: 3; spacing: 10

            GreenSquare {
                BlueSquare {
                    anchors.fill: parent
                    anchors.margins: 8
                    text: '(1)'
                }
            }

            GreenSquare {
                BlueSquare {
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.leftMargin: 8
                    anchors.topMargin: 10
                    text: '(2)'
                }
            }

            GreenSquare {
                BlueSquare {
                    anchors.left: parent.right
                    text: '(3)'
                }
            }

            GreenSquare {
                BlueSquare {
                    id: box
                    height: 24
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: '(4-1)'
                }

                BlueSquare {
                    height: 24
                    anchors.top: box.bottom
                    anchors.topMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: '(4-2)'
                }
            }

            GreenSquare {
                BlueSquare {
                    anchors.centerIn: parent
                    text: '(5)'
                }
            }

            GreenSquare {
                BlueSquare {
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    text: '(6)'
                }
            }
        }
    }
}
