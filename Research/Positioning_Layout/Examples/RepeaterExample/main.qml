import QtQuick 2.9
import QtQuick.Window 2.3
Window {
    id: root
    visible: true

    DarkSquare {
        id: repeat
        width: 252
        height: 252
        property variant colorArray: ["#00bde3", "#67c111", "#ea7025"]


        Grid{
            anchors.fill: parent
            anchors.margins: 8
            spacing: 4
            Repeater {
                model: 16
                Rectangle {
                    width: 56; height: 56
                    property int colorIndex: Math.floor(Math.random()*3)
                    color: repeat.colorArray[colorIndex]
                    border.color: Qt.lighter(color)
                    Text {
                        anchors.centerIn: parent
                        color: "#f0f0f0"
                        text: "Cell " + index
                    }
                }
            }
        }
}
}
