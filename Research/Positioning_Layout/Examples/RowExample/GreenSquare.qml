// GreenSquare.qml

import QtQuick 2.5

Rectangle {
    width: 48
    height: 48
    color: "#67c111"
    border.color: Qt.lighter(color)
}
