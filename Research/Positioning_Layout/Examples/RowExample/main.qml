import QtQuick 2.9
import QtQuick.Window 2.3
Window {
    id: root
    visible: true

    BrightSquare {

        width: 400; height: 120

        Row {
            id: row
            anchors.centerIn: parent
            spacing: 20
            BlueSquare { }
            GreenSquare { }
            RedSquare { }
        }
    }
}
